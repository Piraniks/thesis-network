import os

# Kivy is absurd and not to spam-log requires that kind of hacking. FML.
# Because of this and my iritation the Kivy code is dirty and bad. Sorry.
os.environ["KIVY_NO_FILELOG"] = "1"
os.environ["KIVY_NO_CONFIG"] = "1"
os.environ["KIVY_NO_CONSOLELOG"] = "1"

from kivy.config import Config

Config.set('graphics', 'resizable', False)

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.widget import Widget

from kivy.graphics import Ellipse, Color

from network.simulations import (Simulation, InvasionSimulation,
                                 FailureSimulation, FailureInvasionSimulation,
                                 AttachedSimulation, FailureAttachedSimulation,
                                 InvasionAttachedSimulation,
                                 FailureInvasionAttachedSimulation)
from network import fields

GUI_STARTING_TURN = fields.PositiveIntegerField('GUI_STARTING_TURN')
GUI_TURNS_PER_CLICK = fields.PositiveIntegerField('GUI_TURNS_PER_CLICK')


class GUISimulation(Simulation):
    node_widgets = None
    target_widgets = None


class GUIFailureSimulation(GUISimulation, FailureSimulation):
    pass


class GUIInvasionSimulation(GUISimulation, InvasionSimulation):
    pass


class GUIFailureInvasionSimulation(GUISimulation,
                                   FailureInvasionSimulation):
    pass


class GUIAttachedSimulation(GUISimulation, AttachedSimulation):
    pass


class GUIFailureAttachedSimulation(GUISimulation,
                                   FailureAttachedSimulation):
    pass


class GUIInvasionAttachedSimulation(GUISimulation,
                                    InvasionAttachedSimulation):
    pass


class GUIFailureInvasionAttachedSimulation(GUISimulation,
                                           FailureInvasionAttachedSimulation):
    pass


simulations = {
    'Simulation': GUISimulation,
    'FailureSimulation': GUIFailureSimulation,
    'InvasionSimulation': GUIInvasionSimulation,
    'FailureInvasionSimulation': GUIFailureInvasionSimulation,
    'AttachedSimulation': GUIAttachedSimulation,
    'FailureAttachedSimulation': GUIFailureAttachedSimulation,
    'InvasionAttachedSimulation': GUIInvasionAttachedSimulation,
    'FailureInvasionAttachedSimulation': GUIFailureInvasionAttachedSimulation
}


def update_preview(preview, simulation):
    try:
        for widget in simulation.node_widgets + simulation.target_widgets:
            clear_map_widget(preview=preview, widget=widget)
    except AttributeError:
        pass

    targets = [(target.name, target.position) for target in simulation.targets.active]
    nodes = [(node.name, node.position) for node in simulation.nodes.active]

    width = preview.width
    height = preview.height

    mapped_targets = [
        (name, (position.x * width / simulation.map_size,
                position.y * height / simulation.map_size))
        for name, position in targets
    ]
    mapped_nodes = [
        (name, (position.x * width / simulation.map_size,
                position.y * height / simulation.map_size))
        for name, position in nodes
    ]

    simulation.node_widgets = [NodeWidget(pos=position, id=name)
                               for name, position in mapped_nodes]
    for node_widget in simulation.node_widgets:
        preview.add_widget(node_widget)

    simulation.target_widgets = [TargetWidget(pos=position, id=name)
                                 for name, position in mapped_targets]
    for target_widget in simulation.target_widgets:
        preview.add_widget(target_widget)


def clear_map_widget(preview, widget):
    preview.remove_widget(widget)
    widget.canvas.clear()
    del widget


def move_map_widget(widget, position):
    widget.pos = position
    widget.ellipse.pos = position


class NodeWidget(Widget):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.canvas.clear()

        self.ellipse_color = Color(1., 1., 1.)
        self.canvas.add(self.ellipse_color)

        self.ellipse = Ellipse(size=(5, 5), pos=self.pos)
        self.canvas.add(self.ellipse)


class TargetWidget(Widget):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.canvas.clear()

        self.ellipse_color = Color(1., 0, 0)
        self.canvas.add(self.ellipse_color)

        self.ellipse = Ellipse(size=(5, 5), pos=self.pos)
        self.canvas.add(self.ellipse)


class SimulationPreviewLayout(RelativeLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SimulationMenuLayout(GridLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cols = 1
        self.rows = 9
        self.padding = 10
        self.spacing = 10

        self.name_label = Label(text='Menu', size_hint_y=0.1)
        self.add_widget(self.name_label)

        self.restart_button = Button(text='Start simulation', size_hint_y=0.1)
        self.restart_button.on_press = self.restart_simulation
        self.add_widget(self.restart_button)

        self.step_button = Button(text='Step', size_hint_y=0.1)
        self.step_button.on_press = self.step_simulation
        self.step_button.disabled = True
        self.add_widget(self.step_button)

        self.model_label = Label(text='', size_hint_y=0.1)
        self.add_widget(self.model_label)

        self.turn_label = Label(text='', size_hint_y=0.1)
        self.add_widget(self.turn_label)

        self.nodes_active_label = Label(text='', size_hint_y=0.1)
        self.add_widget(self.nodes_active_label)

        self.active_targets_label = Label(text='', size_hint_y=0.1)
        self.add_widget(self.active_targets_label)

        self.turns_to_spawn_label = Label(text='', size_hint_y=0.1)
        self.add_widget(self.turns_to_spawn_label)

        self.empty_space_label = Label(size_hint_y=0.2)
        self.add_widget(self.empty_space_label)

        self.simulation = None

    def step_simulation(self, steps=None):
        if not hasattr(self.parent, 'simulation'):
            return None
        simulation = self.parent.simulation

        step_per_click = steps if steps is not None else GUI_TURNS_PER_CLICK
        for _ in range(step_per_click._value):
            if simulation.ended is not True:
                simulation.run_next_turn(spawn=True)

        self.turn_label.text = str(f'Turn: {simulation.turn}')
        self.nodes_active_label.text = str(f'Active nodes: {simulation.nodes.active_count}')
        self.active_targets_label.text = str(f'Active targets: {simulation.targets.active_count}')

        target_spawns_in = ((simulation.target_spawn_rate - simulation.turn)
                            % simulation.target_spawn_rate or simulation.target_spawn_rate)
        self.turns_to_spawn_label.text = str(f'Target spawns in: {target_spawns_in}')

        preview = self.parent.preview
        update_preview(preview=preview, simulation=simulation)

    def restart_simulation(self):
        self.restart_button.text = 'Restart simulation'

        if hasattr(self.parent, 'simulation'):
            # Remove last simulation's preview (if exists).
            for widget in self.parent.simulation.node_widgets + self.parent.simulation.target_widgets:
                clear_map_widget(preview=self.parent.preview, widget=widget)

        app = App.get_running_app()
        if hasattr(app, 'simulation_name'):
            chosen_simulation = simulations[app.simulation_name]
        else:
            chosen_simulation = GUISimulation

        self.parent.simulation = chosen_simulation()

        self.simulation = self.parent.simulation
        self.simulation.setup()
        self.simulation.target_widgets = []
        self.simulation.node_widgets = []

        for _ in range(GUI_STARTING_TURN._value):
            self.simulation.run_next_turn(spawn=False)

        preview = self.parent.preview

        self.turn_label.text = str(f'Turn: {self.simulation.turn}')
        self.nodes_active_label.text = str(f'Active nodes: {self.simulation.nodes.active_count}')
        self.active_targets_label.text = str(f'Active targets: {self.simulation.targets.active_count}')

        target_spawns_in = ((self.simulation.target_spawn_rate - self.simulation.turn) %
                            self.simulation.target_spawn_rate or self.simulation.target_spawn_rate)
        self.turns_to_spawn_label.text = str(f'Target spawns in: {target_spawns_in}')

        update_preview(preview=preview, simulation=self.simulation)


class StepSimulationScreen(GridLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cols = 2
        self.rows = 1
        self.padding = 10

        self.menu = SimulationMenuLayout(size_hint_x=1 / 4)
        self.add_widget(self.menu)

        self.preview = SimulationPreviewLayout(size_hint_x=3 / 4)
        self.add_widget(self.preview)


class StepSimulationApp(App):
    title = 'pyWANS'

    def __init__(self, simulation='Simulation'):
        super().__init__()
        self.simulation_name = simulation

    def build(self):
        screen = StepSimulationScreen()
        model_name = self.simulation_name.replace('Simulation', '')
        if not model_name:
            model_name = 'Simulation'

        screen.menu.model_label.text = model_name
        return screen


if __name__ == '__main__':
    StepSimulationApp().run()
