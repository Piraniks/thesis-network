import random
from datetime import datetime
from unittest.mock import patch, MagicMock, call

from freezegun import freeze_time

from network import pywans_random
from network.helpers import random_position, Position
from network.simulations import (Simulation, FailureSimulation, InvasionSimulation,
                                 AttachedSimulation)


class TestSimulation:
    def test_creating_simulation(self):
        simulation = Simulation()

        assert simulation is not None
        assert simulation.environment is not None
        assert simulation.ended is False

    @patch('network.simulations.Simulation.end_simulation_if_no_more_targets')
    def test_end_if_done(self, mend_simulation_if_no_more_targets):
        simulation = Simulation(targets=0)
        simulation.end_if_done()

        mend_simulation_if_no_more_targets.assert_called_once()

    def test_move_nodes_if_node_escaped(self):
        simulation = Simulation()

        target = MagicMock()
        target.is_active = False

        node = MagicMock()
        node.target = target

        nodes = MagicMock()
        nodes.active = [node]

        simulation.nodes = nodes

        simulation.move_nodes()

        assert node.target is None

    def test_end_simulation_if_no_more_targets(self):
        simulation = Simulation(targets=0)
        simulation.end_simulation_if_no_more_targets()

        assert simulation.ended is True

    def test_setup(self):
        node_number = 10
        simulation = Simulation(nodes=node_number)
        simulation.setup()

        assert simulation.node_number == node_number
        assert simulation.nodes.count == node_number

    def test_spawn_target_on_starting_turn(self):
        simulation = Simulation(target_spawn_rate=10)
        simulation.spawn_target()

        assert simulation.targets.count == 1
        assert simulation.targets.active_count == 1

    def test_spawn_target_with_not_aligned_turn(self):
        simulation = Simulation(target_spawn_rate=10)
        simulation.turn = 5
        simulation.spawn_target()

        assert simulation.targets.count == 0
        assert simulation.targets.active_count == 0

    def test_spawn_target_with_aligned_turn(self):
        simulation = Simulation(target_spawn_rate=1)
        simulation.turn = 5
        simulation.spawn_target()

        assert simulation.targets.active_count == 1
        assert simulation.targets.count == 1

    def test_spawn_target_random_positions(self):
        # set seed for random position function
        seed = random.randint(0, 1000)
        pywans_random.seed(seed)

        simulation = Simulation(target_spawn_rate=10)
        simulation.spawn_target()
        simulation.spawn_target()

        map_size = simulation.environment.map_size
        # restart seed to get the same values
        pywans_random.seed(seed)

        position1 = random_position(1, map_size - 1)
        position2 = random_position(1, map_size - 1)

        targets = simulation.targets.active
        target1, target2 = targets

        assert target1.position == position1
        assert target2.position == position2
        assert target1.position != target2.position

    def test_nodes_pursue_when_node_is_close(self):
        simulation = Simulation()

        node = simulation.nodes.create(position=(50, 51))
        target = simulation.targets.create(position=(50, 50))

        node_target_before = node.target

        simulation.nodes_pursue()

        node_target_after = node.target

        assert target in simulation.targets.all
        assert target in simulation.targets.active
        assert node_target_before is None
        assert node_target_after == target
        assert target.pursued_by is node

    def test_nodes_pursue_when_node_is_away(self):
        simulation = Simulation()

        node = simulation.nodes.create(position=(90, 90))
        target = simulation.targets.create(position=(50, 50))

        node_target_before = node.target

        simulation.nodes_pursue()

        node_target_after = node.target

        assert target in simulation.targets.active
        assert node_target_before is None
        assert node_target_after is None
        assert target.pursued_by is None

    def test_nodes_pursue_when_target_is_pursued(self):
        simulation = Simulation()

        node = simulation.nodes.create(position=(50, 51))
        pursuer = simulation.nodes.create(position=(52, 52))
        target = simulation.targets.create(position=(50, 50))
        target.pursued_by = pursuer

        node_target_before = node.target

        simulation.nodes_pursue()

        node_target_after = node.target

        assert target in simulation.targets.active
        assert node_target_before is None
        assert node_target_after is None
        assert target.pursued_by is pursuer

    def test_age_targets(self):
        simulation = Simulation()

        target1 = simulation.targets.create(position=(50, 50))
        simulation.age_targets()

        target2 = simulation.targets.create(position=(50, 50))

        simulation.age_targets()

        assert target1.lifetime == 2
        assert target2.lifetime == 1

    def test_age_targets_deactivate_target_without_pursuer(self):
        simulation = Simulation()
        max_lifetime = random.randint(10, 20)

        target_old = simulation.targets.create(position=(50, 50),
                                               max_lifetime=max_lifetime,
                                               )
        target_old.lifetime = max_lifetime - 1

        simulation.age_targets()

        assert target_old.caught is False
        assert target_old in simulation.targets.all
        assert target_old not in simulation.targets.active
        assert target_old.lifetime == max_lifetime

    def test_age_targets_deactivate_target_with_pursuer(self):
        simulation = Simulation()
        pursuer = simulation.nodes.create(position=(50, 50))
        max_lifetime = random.randint(10, 20)

        target_old_pursued = simulation.targets.create(position=(50, 50),
                                                       max_lifetime=max_lifetime)
        target_old_pursued.lifetime = max_lifetime - 1
        pursuer.target = target_old_pursued
        target_old_pursued.pursued_by = pursuer

        simulation.age_targets()

        assert target_old_pursued.caught is False
        assert target_old_pursued in simulation.targets.all
        assert target_old_pursued not in simulation.targets.active
        assert target_old_pursued.lifetime == max_lifetime
        assert target_old_pursued.pursued_by is None
        assert pursuer.target is None

    def test_move_nodes_by_loads(self):
        simulation = Simulation()
        node = simulation.nodes.create(position=(60, 60))

        node_position_before_x = node.position.x
        node_position_before_y = node.position.y
        simulation.move_nodes()

        assert node.position.x < node_position_before_x
        assert node.position.y < node_position_before_y

    def test_move_nodes_by_loads_equilibrium(self):
        simulation = Simulation()

        position1 = (simulation.environment.map_size / 3,
                     50)
        position2 = (simulation.environment.map_size * 2 / 3,
                     50)

        node1 = simulation.nodes.create(position=position1)
        node2 = simulation.nodes.create(position=position2)

        simulation.move_nodes()

        assert node1.position.y == Position(*position1).y
        assert node2.position.y == Position(*position2).y

    def test_move_nodes_by_loads_with_moving_nodes(self):
        simulation = Simulation()
        node = simulation.nodes.create(position=(50, 50),
                                       sight_range=20, load=10)
        target = simulation.targets.create(position=(90, 90))

        node_moving = simulation.nodes.create(position=(45, 45),
                                              sight_range=20, load=10)
        node_moving.target = target

        node_position_before = Position(*node.position)
        simulation.move_nodes()

        assert node.position != node_position_before

    def test_move_nodes_deactivate_target(self):
        simulation = Simulation()
        node = simulation.nodes.create(position=(50, 50), speed=6,
                                       sight_range=20, load=10)
        target = simulation.targets.create(position=(51, 51))

        node.target = target

        simulation.move_nodes()

        assert node.position == target.position
        assert node.target is None

    def test_move_nodes_move_towards_target(self):
        simulation = Simulation()
        node = simulation.nodes.create(position=(50, 50))
        target = simulation.targets.create(position=(90, 90))

        node.target = target
        node_position_before = Position(node.position.x, node.position.y)

        simulation.move_nodes()

        assert node.position.x > node_position_before.x
        assert node.position.y > node_position_before.y
        assert node.target is target

    def test_run_next_turn(self):
        simulation = Simulation()

        node1 = simulation.nodes.create(position=(50, 50),
                                        sight_range=20,
                                        speed=2)
        node2 = simulation.nodes.create(position=(49, 49),
                                        sight_range=20,
                                        speed=2)
        target = simulation.targets.create(position=(55, 55))

        turn_before = simulation.turn
        target_age_before = target.lifetime
        node1_position_before = Position(*node1.position)
        node2_position_before = Position(*node2.position)

        simulation.run_next_turn(spawn=False)

        assert simulation.turn == turn_before + 1
        assert simulation.ended is False
        assert target.lifetime == target_age_before + 1
        assert node1.position != node1_position_before
        assert node1.target == target
        assert node2.position != node2_position_before
        assert node2.target is None

    def test_run_next_turn_if_no_more_targets(self):
        simulation = Simulation(targets=0)

        simulation.nodes.create(position=(50, 50), sight_range=20, speed=2)

        simulation.run_next_turn()

        assert simulation.ended is True

    def test_run_next_turn_if_0_targets_but_more_to_spawn(self):
        simulation = Simulation(targets=2, target_spawn_rate=200)

        simulation.nodes.create(position=(50, 50), sight_range=20, speed=2)

        simulation.run_next_turn()
        simulation.run_next_turn()

        target = simulation.targets.all[0]
        simulation.targets.delete(target)

        assert simulation.ended is False

    def test_run_next_turn_with_spawn_false(self):
        simulation = Simulation(targets=1, target_spawn_rate=1)

        simulation.nodes.create(position=(50, 50), sight_range=20, speed=2)

        simulation.run_next_turn(spawn=False)
        simulation.run_next_turn(spawn=False)
        simulation.run_next_turn(spawn=False)

        assert simulation.ended is False

    def test_make_actions_targets_with_targets_with_actions(self):
        simulation = Simulation(targets=1, target_spawn_rate=1)

        simulation.targets.create(position=(50, 50))
        target = simulation.targets.active[0]
        target.has_actions = True
        target.perform_actions = MagicMock()

        simulation.make_actions_targets()

        target.perform_actions.assert_called_once_with(simulation)

    def test_results(self):
        nodes = MagicMock()
        targets = MagicMock()

        simulation = Simulation(targets=1, target_spawn_rate=1)
        simulation.nodes = nodes
        simulation.targets = targets

        frozen_datetime = datetime.now()

        with freeze_time(frozen_datetime):
            assert simulation.results == {
                'nodes': nodes.as_json,
                'targets': targets.as_json,
                'time_taken': str(datetime.now() - simulation.start_datetime)
            }

    def test_as_json_for_ended_simulation(self):
        simulation = Simulation(targets=1, target_spawn_rate=1)
        simulation.setup()

        frozen_datetime = datetime.now()

        with freeze_time(frozen_datetime):
            assert simulation.as_json == {
                'model': type(simulation).__name__,
                'node_number': simulation.node_number,
                'target_spawn_rate': simulation.target_spawn_rate,
                'target_number': simulation.target_number,
                'targets_to_spawn': simulation.targets_to_spawn,
                'turn': simulation.turn,
                'environment': simulation.environment.as_json,
                'ended': simulation.ended
            }

    def test_as_json_for_not_ended_simulation(self):
        simulation = Simulation(targets=1, target_spawn_rate=1)
        simulation.setup()
        simulation.ended = True

        frozen_datetime = datetime.now()

        with freeze_time(frozen_datetime):
            assert simulation.as_json == {
                'model': type(simulation).__name__,
                'node_number': simulation.node_number,
                'target_spawn_rate': simulation.target_spawn_rate,
                'target_number': simulation.target_number,
                'targets_to_spawn': simulation.targets_to_spawn,
                'turn': simulation.turn,
                'environment': simulation.environment.as_json,
                'ended': simulation.ended,
                'time_taken': str(datetime.now() - simulation.start_datetime),
                'nodes': simulation.nodes.as_json,
                'targets': simulation.targets.as_json
            }


class TestInvasionSimulation:
    @patch('network.simulations.random_sidemap_position')
    def test_spawn_target_with_given_generated_position(self,
                                                        mrandom_sidemap_position):
        mrandom_sidemap_position.return_value = random_position()
        seed = random.randint(0, 1000)
        pywans_random.seed(seed)
        simulation = InvasionSimulation(target_spawn_rate=200, nodes=10)

        simulation.spawn_target()
        target = simulation.targets.active[0]

        assert target.position == Position(*mrandom_sidemap_position.return_value)

    def test_age_targets(self):
        seed = random.randint(0, 1000)
        pywans_random.seed(seed)
        active_targets = [MagicMock(), MagicMock()]
        targets = MagicMock()
        targets.active = active_targets
        simulation = InvasionSimulation()
        simulation.targets = targets

        simulation.age_targets()

        for mtarget in active_targets:
            mtarget.age.assert_called_once_with(1)

        targets.all.assert_not_called()

    @patch('network.simulations.Simulation.end_if_done')
    def test_end_if_done_deactivates_targets_in_the_middle_of_the_map(self,
                                                                      mend_if_done):
        simulation = InvasionSimulation(target_spawn_rate=200, nodes=10)

        target_in_the_centre = simulation.targets.create(position=simulation.map_centre)
        target_not_in_the_centre = simulation.targets.create(position=(simulation.map_centre.x - 1,
                                                                       simulation.map_centre.y - 1))

        simulation.end_if_done()

        mend_if_done.assert_called_once()
        assert target_in_the_centre.is_active is False
        assert target_in_the_centre.caught is False
        assert target_not_in_the_centre.is_active is True


class TestFailureSimulation:

    def test_setup_with_percentage(self):
        simulation = FailureSimulation(target_spawn_rate=200, nodes=10)

        simulation.setup(number=0.3)

        inactive_nodes = [node for node in simulation.nodes.all if
                          node.is_active is False]

        assert len(inactive_nodes) == 3
        assert simulation.nodes.active_count == 7

    def test_partial_failure_with_percentage(self):
        simulation = FailureSimulation(target_spawn_rate=200, nodes=10)
        for _ in range(simulation.node_number):
            simulation.nodes.create(is_active=True)

        simulation.partial_failure(number=0.3)

        inactive_nodes = [node for node in simulation.nodes.all if
                          node.is_active is False]

        assert len(inactive_nodes) == 3
        assert simulation.nodes.active_count == 7

    def test_partial_failure_with_number(self):
        simulation = FailureSimulation(target_spawn_rate=200, nodes=10)
        for _ in range(simulation.node_number):
            simulation.nodes.create(is_active=True)

        simulation.partial_failure(3)

        inactive_nodes = [node for node in simulation.nodes.all if
                          node.is_active is False]

        assert len(inactive_nodes) == 3
        assert simulation.nodes.active_count == 7

    def test_run_next_turn(self):
        simulation = FailureSimulation()

        node1 = simulation.nodes.create(position=(50, 50),
                                        sight_range=20,
                                        speed=2)
        node2 = simulation.nodes.create(position=(49, 49),
                                        sight_range=20,
                                        speed=2)
        target = simulation.targets.create(position=(55, 55))

        turn_before = simulation.turn
        node1_position_before = Position(*node1.position)
        node2_position_before = Position(*node2.position)

        simulation.run_next_turn()

        # Remove created target
        created_target = next(target_ for target_ in
                              simulation.targets.all
                              if target_ is not target)
        simulation.targets.delete(created_target)

        assert simulation.turn == turn_before + 1
        assert simulation.ended is False
        assert node1.position != node1_position_before
        assert node1.target == target
        assert node2.position != node2_position_before
        assert node2.target is None


class TestAttachedSimulation:
    @patch('network.simulations.random_position')
    def test_position_generator_returns_square_and_later_randoms(self,
                                                                 mrandom_position):
        """
        Attached Simulation should return `square` with positions on the map and
        then if more nodes are to be created - those should receive random positions.
        """
        map_size = 6
        node_number = 5

        simulation = AttachedSimulation(nodes=node_number)
        menvironment = MagicMock(map_size=map_size)
        simulation.environment = menvironment

        expected_positions = [
            Position(2, 2), Position(2, 4), Position(4, 2), Position(4, 4),
            mrandom_position.return_value
        ]

        position_generator = simulation.position_generator
        returned_positions = [next(position_generator) for _
                              in range(node_number)]

        assert expected_positions == returned_positions

    @patch('network.simulations.AttachedSimulation.position_generator')
    def test_setup_calls_initializes_positions(self,
                                               mposition_generator):
        node_number = random.randint(4, 16)
        chosen_positions = [random_position() for _ in range(node_number)]
        simulation = AttachedSimulation(nodes=node_number)
        mnodes = MagicMock()
        simulation.nodes = mnodes
        mposition_generator.__next__.side_effect = chosen_positions

        simulation.setup()

        expected_create_calls = [
            call(default_position=position, position=position)
            for position in chosen_positions
        ]
        expected_generator_calls = [call() for _ in range(node_number)]

        assert mposition_generator.__next__.mock_calls == expected_generator_calls
        assert mnodes.create.mock_calls == expected_create_calls

    def test_move_nodes_with_no_target_and_node_on_default_position(self):
        simulation = AttachedSimulation(nodes=1)
        default_position = random_position()
        mnode = MagicMock(target=None,
                          position=default_position,
                          default_position=default_position)
        mnodes = MagicMock(active=[mnode])
        simulation.nodes = mnodes

        simulation.move_nodes()

        mnode.assert_not_called()

    def test_move_nodes_with_no_target_and_node_not_on_default_position(self):
        simulation = AttachedSimulation(nodes=1)
        default_position = random_position()
        position = random_position()
        mnode = MagicMock(target=None,
                          position=position,
                          default_position=default_position)
        mnodes = MagicMock(active=[mnode])
        simulation.nodes = mnodes

        simulation.move_nodes()

        mnode.move_to_position.assert_called_once_with(default_position)

    def test_move_nodes_with_target_that_escaped(self):
        simulation = AttachedSimulation(nodes=1)
        default_position = random_position()
        position = random_position()
        mtarget = MagicMock(is_active=False)
        mnode = MagicMock(target=mtarget,
                          position=position,
                          default_position=default_position)
        mnodes = MagicMock(active=[mnode])
        mtargets = MagicMock(active=[mtarget])
        simulation.nodes = mnodes
        simulation.targets = mtargets

        simulation.move_nodes()

        assert mnode.target is None
        mnode.move_to_position.assert_called_once_with(default_position)

    def test_move_nodes_with_active_target_in_range(self):
        speed = 2
        simulation = AttachedSimulation(nodes=1)
        default_position = random_position()
        position = random_position()
        mtarget = MagicMock(is_active=True)
        mnode = MagicMock(target=mtarget,
                          position=position,
                          default_position=default_position,
                          speed=speed)
        mnode.distance.return_value = speed - 1
        mnodes = MagicMock(active=[mnode])
        mtargets = MagicMock(active=[mtarget])
        simulation.nodes = mnodes
        simulation.targets = mtargets

        simulation.move_nodes()

        assert mnode.target is None
        mnode.move_to_position.assert_called_once_with(mtarget.position)
        mtargets.deactivate.assert_called_once_with(mtarget)

    def test_move_nodes_with_active_target_outside_the_range(self):
        speed = 2
        simulation = AttachedSimulation(nodes=1)
        default_position = random_position()
        position = random_position()
        mtarget = MagicMock(is_active=True)
        mnode = MagicMock(target=mtarget,
                          position=position,
                          default_position=default_position,
                          speed=speed)
        mnode.distance.return_value = speed + 1
        mnodes = MagicMock(active=[mnode])
        mtargets = MagicMock(active=[mtarget])
        simulation.nodes = mnodes
        simulation.targets = mtargets

        simulation.move_nodes()

        assert mnode.target is mtarget
        mnode.move_to_position.assert_called_once_with(mtarget.position)
        mtargets.deactivate.assert_not_called()
