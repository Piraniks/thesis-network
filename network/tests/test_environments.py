import random
from unittest.mock import sentinel

import pytest

from network import pywans_random
from network.environments import Environment
from network.helpers import Force, sign
from network.managers import TargetManager, NodeManager


class TestEnvironment:
    def test_environment_destroy_method(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(nodes=nodes, targets=targets)

        target = targets.create()
        node = nodes.create()

        assert environment.targets.all == [target]
        assert environment.nodes.all == [node]

    @pytest.mark.parametrize('data', [
        [(50, 50), Force(0, 0)],
        [(50, 90), Force(0, -1)],
        [(90, 50), Force(-1, 0)],
        [(90, 90), Force(-1, -1)],
        [(10, 90), Force(1, -1)],
        [(90, 10), Force(-1, 1)],
        [(10, 10), Force(1, 1)],
        [(50, 10), Force(0, 1)],
        [(10, 50), Force(1, 0)],
    ])
    def test_environment_wall_forces(self, data):
        position, force_signs = data
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = environment.nodes.create(position=position)

        wall_force = environment.get_walls_forces(node)

        assert sign(wall_force.x) == sign(force_signs.x)
        assert sign(wall_force.y) == sign(force_signs.y)

    def test_get_bottom_force(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node_close = nodes.create(position=(50, 90))
        node_afar = nodes.create(position=(50, 10))

        bottom_force_close = environment.get_bottom_force(node_close)
        bottom_force_afar = environment.get_bottom_force(node_afar)

        assert sign(bottom_force_close.y) == -1
        assert sign(bottom_force_close.x) == 0
        assert sign(bottom_force_afar.y) == -1
        assert sign(bottom_force_afar.x) == 0
        assert abs(bottom_force_close.y) > abs(bottom_force_afar.y)

    def test_get_top_force(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node_close = nodes.create(position=(50, 10))
        node_afar = nodes.create(position=(50, 90))

        bottom_force_close = environment.get_top_force(node_close)
        bottom_force_afar = environment.get_top_force(node_afar)

        assert sign(bottom_force_close.y) == 1
        assert sign(bottom_force_close.x) == 0
        assert sign(bottom_force_afar.y) == 1
        assert sign(bottom_force_afar.x) == 0
        assert abs(bottom_force_close.y) > abs(bottom_force_afar.y)

    def test_get_left_force(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node_close = nodes.create(position=(10, 50))
        node_afar = nodes.create(position=(90, 50))

        bottom_force_close = environment.get_left_force(node_close)
        bottom_force_afar = environment.get_left_force(node_afar)

        assert sign(bottom_force_close.x) == 1
        assert sign(bottom_force_close.y) == 0
        assert sign(bottom_force_afar.x) == 1
        assert sign(bottom_force_afar.y) == 0
        assert abs(bottom_force_close.x) > abs(bottom_force_afar.x)

    def test_get_right_force(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node_close = nodes.create(position=(90, 50))
        node_afar = nodes.create(position=(10, 50))

        bottom_force_close = environment.get_right_force(node_close)
        bottom_force_afar = environment.get_right_force(node_afar)

        assert sign(bottom_force_close.x) == -1
        assert sign(bottom_force_close.y) == 0
        assert sign(bottom_force_afar.x) == -1
        assert sign(bottom_force_afar.y) == 0
        assert abs(bottom_force_close.x) > abs(bottom_force_afar.x)

    def test_environment_wall_forces_with_forces_methods(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = nodes.create()

        force = Force.sum(
            environment.get_top_force(node),
            environment.get_bottom_force(node),
            environment.get_left_force(node),
            environment.get_right_force(node),
        )

        wall_force = environment.get_walls_forces(node)

        assert force == wall_force

    def test_get_node_force(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        obj_position = (40, 40)
        obj_load = 10
        obj = nodes.create(position=obj_position, load=obj_load)

        node = nodes.create(position=(50, 50), load=10)

        force = environment.get_node_force(node, obj)
        opposive_force = environment.get_node_force(obj, node)

        assert force == -opposive_force

    def test_get_node_force_with_the_same_position(self):
        # set seed for random position function
        seed = random.randint(0, 1000)
        pywans_random.seed(seed)
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        position = (40, 40)
        load = 10
        obj = nodes.create(position=position, load=load)
        node = nodes.create(position=position, load=load)

        node_force = environment.get_node_force(node, obj)
        obj_force = environment.get_node_force(obj, node)

        # restart seed to get the same values
        pywans_random.seed(seed)
        added_node_force = pywans_random.uniform(-1, 1), pywans_random.uniform(-1, 1)
        added_obj_force = pywans_random.uniform(-1, 1), pywans_random.uniform(-1, 1)

        obj_force_without_added = obj_force - Force(*added_obj_force)
        node_forge_without_added = node_force - Force(*added_node_force)

        assert node_force != obj_force
        assert obj_force_without_added == node_forge_without_added

    def test_get_node_force_increases_with_load(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        obj_position = (45, 45)
        obj1_load = 10
        obj1 = nodes.create(position=obj_position, load=obj1_load)

        obj2_load = 20
        obj2 = nodes.create(position=obj_position, load=obj2_load)

        node = nodes.create(position=(50, 50), load=10)

        force1 = environment.get_node_force(node, obj1)
        force2 = environment.get_node_force(node, obj2)

        assert abs(force1.x) < abs(force2.x)
        assert abs(force1.y) < abs(force2.y)

    def test_get_node_force_decreases_with_distance(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        obj_load = 10
        obj1_position = (45, 45)
        obj1 = nodes.create(position=obj1_position, load=obj_load)

        obj2_position = (40, 40)
        obj2 = nodes.create(position=obj2_position, load=obj_load)

        node = nodes.create(position=(50, 50), load=10)

        force1 = environment.get_node_force(node, obj1)
        force2 = environment.get_node_force(node, obj2)

        assert abs(force1.x) > abs(force2.x)
        assert abs(force1.y) > abs(force2.y)

    def test_get_nodes_forces(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = nodes.create(position=(50, 50), load=10)

        influencing_node1 = nodes.create(position=(40, 40), load=10)
        influencing_node2 = nodes.create(position=(60, 60), load=10)

        nodes_force = environment.get_nodes_forces(node)

        force1 = environment.get_node_force(node, influencing_node1)
        force2 = environment.get_node_force(node, influencing_node2)

        assert nodes_force == force1 + force2

    def test_get_nodes_forces_with_global_forces(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)
        environment.global_load = True

        node = nodes.create(position=(50, 50), load=10)

        influencing_node1 = nodes.create(position=(40, 40), load=10)
        influencing_node2 = nodes.create(position=(90, 90), load=10)

        nodes_force = environment.get_nodes_forces(node)

        force1 = environment.get_node_force(node, influencing_node1)
        force2 = environment.get_node_force(node, influencing_node2)

        assert nodes_force == force1 + force2

    def test_get_force_for_only_one_node_in_centre(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = nodes.create(position=(50, 50), load=10)

        force = environment.calculate_force(node)

        assert force == Force(0, 0)

    def test_get_force_for_only_one_node_not_in_the_centre(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = nodes.create(position=(50, 60), load=10)

        force = environment.calculate_force(node)

        # node should be moved by forces left only as it's to the right from the centre
        expected_force_signs = (0, -1)
        force_signs = sign(force.x), sign(force.y)

        assert force != Force(0, 0)
        assert expected_force_signs == force_signs

    def test_get_force_for_node(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        node = nodes.create(position=(50, 50), load=10)

        nodes.create(position=(40, 40), load=10)
        nodes.create(position=(60, 60), load=10)

        nodes_force = environment.get_nodes_forces(node)
        walls_force = environment.get_walls_forces(node)

        force = environment.calculate_force(node)

        assert force == nodes_force + walls_force

    def test_get_target_by_position(self):
        targets = TargetManager()
        nodes = NodeManager()

        environment = Environment(targets=targets, nodes=nodes)

        position = (50, 50)
        target = targets.create(position=position)

        target_found = environment.get_target_by_position(position)

        assert target == target_found

    def test_as_json(self):
        k = sentinel.k
        map_size = sentinel.map_size
        wall_load = sentinel.wall_load

        environment = Environment(k=k, map_size=map_size,
                                  wall_load=wall_load)

        assert environment.as_json == {
            'K': k,
            'map_size': map_size,
            'wall_load': wall_load
        }
